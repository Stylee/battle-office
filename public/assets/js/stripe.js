// strip
////////

var stripe = Stripe('pk_test_2tiNSRfnCsFXwwCliOGEKG1Z00XTKhn3aL');
var elements = stripe.elements();


// Custom styling can be passed to options when creating an Element.
var style = {
  base: {
    // Add your base input styles here. For example:
    fontSize: '16px',
    color: "#32325d",
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.

card.mount('#card-element');
// window.addEventListener("onload", function(){

// }); 

// Elements validates user input as it is typed. To help your
// customers catch mistakes, you should listen to change events on the
// card Element and display any errors:

card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});


// The payment details collected using Elements can then be converted
// into a token. Create an event handler that handles the submit event
// on the form. The handler sends the fields to Stripe for
// tokenization and prevents the form’s submission (the form is
// submitted by JavaScript in the next step).

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});


// The last step is to submit the token, along with any additional
// information that has been collected, to your server.

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Pour que symfony soit content
    // var hiddenInputStripeToken = document.createElement('input');
    // hiddenInputStripeToken.setAttribute('type', 'hidden');
    // hiddenInputStripeToken.setAttribute('name', 'order_form[stripeToken]');
    // hiddenInputStripeToken.setAttribute('value', token.id);
    // form.appendChild(hiddenInputStripeToken);
    
    // Pour que symfony soit content encore
    var hiddenInputPaymentMode = document.createElement('input');
    hiddenInputPaymentMode.setAttribute('type', 'hidden');
    hiddenInputPaymentMode.setAttribute('name', 'order_form[paymentMode]');
    hiddenInputPaymentMode.setAttribute('value', "stripe");
    form.appendChild(hiddenInputPaymentMode);

    
  // Submit the form
  form.submit();
}
