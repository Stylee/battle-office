<?php

namespace App\Repository;

use App\Entity\OrderRequests;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrderRequests|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderRequests|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderRequests[]    findAll()
 * @method OrderRequests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRequestsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderRequests::class);
    }

    // /**
    //  * @return OrderRequests[] Returns an array of OrderRequests objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderRequests
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
