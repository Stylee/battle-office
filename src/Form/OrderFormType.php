<?php

namespace App\Form;

use App\Entity\OrderRequests;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Adresses;
use App\Form\AdressesFormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Valid;


// use Symfony\Component\Validator\Constraints\Collection
use Symfony\Component\Form\Extension\Core\Type\CollectionType;



class OrderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paymentMode')
            ->add('facturationAdress', AdressesFormType::class
                  // , [
                // 'constraints' => array(new Valid())
            // ]
            )
            ->add('deliveryAdress', AdressesFormType::class, [
                // 'constraints' => array(new Valid()),
                'required' => false
            ])

            ->add('idOfProduct', IntegerType::class, array('mapped' => false) )
            ->add('stripeToken', TextType::class, array('mapped' => false))
            // ->add('idProduct', integerType::class)
        ;
	
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        // $resolver->setDefaults([
        
        // ]);

        $resolver->setDefaults(array(
            'data_class' => OrderRequests::class,
            'cascade_validation' => true,
        ));
    }

}
